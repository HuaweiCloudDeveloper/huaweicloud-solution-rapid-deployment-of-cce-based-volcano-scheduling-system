[TOC]

**解决方案介绍**
===============
该解决方案能帮您快速在华为云CCE容器集群上快速部署Volcano批量调度系统，在原生 K8s 的基础上，补齐了Job调度和设备管理等多方面的短板，为客户提供通用、可扩展、高性能、稳定的原生批量计算平台，方便以 Kubeflow 、 KubeGene 、 Spark 为代表的上层业务组件集成和使用。通过与 Volcano 的集成，可以同时调度 Flink 的作业和任务管理器，适用于资源不足的集群。

解决方案实践详情页面：[https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-cce-based-volcano-scheduling-system.html](https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-cce-based-volcano-scheduling-system.html)

**架构图**
---------------

![方案架构](./document/rapid-deployment-of-cce-based-volcano-scheduling-system.PNG)

**架构描述**
---------------

该解决方案会部署如下资源：
1. 创建3个控制节点的高可用云容器引擎CCE集群，提供计算节点的纳管和业务系统的管理调度能力。
2. 部署1个弹性云服务器ECS节点，作为集群的计算节点，用于承载业务系统。
3. 部署1个弹性云服务器ECS节点，作为集群的计算节点，用于承载业务系统。
4. 部署1个弹性云服务器ECS节点，作为集群的计算节点，用于承载业务系统。

**组织结构**
---------------

``` lua
huaweicloud-solution-rapid-deployment-of-cce-based-volcano-scheduling-system
├── huaweicloud-solution-rapid-deployment-of-cce-based-volcano-scheduling-system.tf.json -- 资源编排模板
```
**开始使用**
---------------
1、登录ECS[弹性云服务器](https://console.huaweicloud.com/ecm/?region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)控制平台，选择{ecs_name}-manager01主管理节点的弹性云服务器，单击远程登录，或者使用其他的远程登录工具进入Linux弹性云服务器。

图1 登录ECS云服务器控制平台
![登录ECS云服务器控制平台](./document/readme-image-001.PNG)

图2 登录ECS云服务器
![登录ECS云服务器](./document/readme-image-002.PNG)

2、在Linux弹性云服务中输入账号和密码后回车。

图3 登录ECS弹性云服务器
![登录](./document/readme-image-003.PNG)

3、登录云容器引擎CCE平台，选择CCE集群，查看kubectl连接集群方式。

图4 查看集群状态信息，并查看kubectl连接集群方式
![查看集群状态信息，并查看kubectl连接集群方式](./document/readme-image-004.PNG)
![查看集群状态信息，并查看kubectl连接集群方式](./document/readme-image-005.PNG)
![查看集群状态信息，并查看kubectl连接集群方式](./document/readme-image-006.PNG)

4、部署Flink operator。

图5 查看集群连接情况

![查看集群连接情况](./document/readme-image-007.PNG)

执行：kubectl apply -f cert-manager.yaml
kubectl -n cert-manager get pod

图6 部署cert-manager
![部署cert-manager](./document/readme-image-008.PNG)

执行：kubectl apply -f flink-operator.yaml
kubectl -n flink-operator-system get pod,svc

图7 部署Flink Operator
![部署Flink Operator](./document/readme-image-009.PNG)

5、pathFlink JobCluster 测试

kubectl create -f flinkoperator_v1beta1_flinkjobcluster_volcano.yaml  --validate=false
kubectl get pod,svc

图8 部署Flink JobCluster
![部署Flink Operator](./document/readme-image-010.PNG)

kubectl get pod

图8 确认Job正确执行完成
![确认Job正确执行完成](./document/readme-image-011.PNG)

图9 确认Job由volcano调度
![确认Job由volcano调度](./document/readme-image-012.PNG)
